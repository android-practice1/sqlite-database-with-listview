# SQLite with ListView C114

**Code Explanation** 
 1. Add records in database 
 2. Show records in ListView 
 3. Fetch records as **Name** form ListView for update records 
 4. Delete records from ListView 

1. Class 
    * DatabaseHelper
      
        > Used for SQLite query.
    * EditDataActivity 
        > This calls has to deal with `save` and `delete` **button**
        >
        > and this has also connection with `edit_data_layout`
    * ListDataActivity
      
        > It has to deal with `list_layout` and Displaying data in the ListView. 
    * MainActivity
      
        > work with `add` and `view` **button**  
2. Layout
    * activity_main
      
        > It has **1 EditText** and **2 Button (add, view)**
    * edit_data_layout
        > ```xml
        > <ListView
        >      android:layout_width="match_parent"
        >      android:layout_height="match_parent"
        >      android:id="@+id/listView">
        > </ListView>
        > ```
    * list_layout
    
        > It has **1 EditText** and **2 Button (delete, save)** 

**Java Methods**
 1. Add Data 
 ```java
    // adding data
    public boolean addData(String item) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAME, item);

        long result = sqLiteDatabase.insert(TABLE_NAME, null, contentValues);

        //if date as inserted incorrectly it will return -1
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }
 ```
 2. Get ID for update 
 ```java 
    // Fetch data from database
    public Cursor getData(){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor data = db.rawQuery(query, null);
        return data;
    }
 ```
 3. Update method 
 ```java
 // Updates the name field
    public void updateName(String newName, int id, String oldName){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE " + TABLE_NAME + " SET " + NAME +
                " = '" + newName + "' WHERE " + ID + " = '" + id + "'" +
                " AND " + NAME + " = '" + oldName + "'";
        db.execSQL(query);
    }
 ```
 4. Delete Method 
 ```java
 // Delete from database
    public void deleteName(int id, String name){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + TABLE_NAME + " WHERE "
                + ID + " = '" + id + "'" +
                " AND " + NAME + " = '" + name + "'";
        db.execSQL(query);
    }
 ```

### TESTED AND OK

Farhan Sadik

