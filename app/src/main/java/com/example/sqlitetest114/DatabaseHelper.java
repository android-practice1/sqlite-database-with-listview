package com.example.sqlitetest114;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "test.db"; // database name
    private static final String TABLE_NAME = "student_details"; // table name
    private static final String ID = "id"; // primary key
    private static final String NAME = "Name"; // column
    private static final String AGE = "Age"; // column
    private static final int VERSION_NUMBER = 1; // version number
    // create table query
    private static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME +
            "(id INTEGER PRIMARY KEY AUTOINCREMENT, "+
            NAME + " VARCHAR(25), " + AGE + " INTEGER);";
    // drop table query
    private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;

    // constructor
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION_NUMBER);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE); // creating table
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE); // deleting table
        onCreate(db); // creating table
    }

    // adding data
    public boolean insertData(String name, String age) {

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        // putkey
        contentValues.put(NAME, name);
        contentValues.put(AGE, age);

        long result = sqLiteDatabase.insert(TABLE_NAME, null, contentValues);

        // if date as inserted incorrectly it will return -1
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    // Fetch data from database
    public Cursor displayData(){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor data = db.rawQuery(query, null);
        return data;
    }

    // Returns only the ID that matches the name passed in
    public Cursor getItemID(String nameString){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + ID + " FROM " + TABLE_NAME +
                " WHERE " + NAME + " = '" + nameString + "'";

        Cursor cursor = db.rawQuery(query, null);
        return cursor;
    }


    // Updates the name field
    /*
    public void updateName(String newName, int id, String oldName, String ){

        // multiple items
        // UPDATE salery SET Name = 'Galib', salery = 2800 WHERE id = 2 AND Name = 'Mula';

        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE " + TABLE_NAME + " SET " + NAME +
                " = '" + newName + "' WHERE " + ID + " = '" + id + "'" +
                " AND " + NAME + " = '" + oldName + "'";
        db.execSQL(query);
    }
     */

    public void updateName(int id, String newName, String newAge){

        // multiple items
        // UPDATE salery SET Name = 'Galib', salery = 2800 WHERE id = 2 AND Name = 'Mula';

        SQLiteDatabase db = this.getWritableDatabase();

        String newQuery = "UPDATE " + TABLE_NAME + " SET " + NAME + " = '" + newName + "', " +
                AGE + " = " + newAge + " WHERE " + ID + " = " + id + ";" ;
        db.execSQL(newQuery);
    }


}
