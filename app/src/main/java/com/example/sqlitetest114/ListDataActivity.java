package com.example.sqlitetest114;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListDataActivity extends AppCompatActivity {

    DatabaseHelper databaseHelper;
    ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_data);

        list = findViewById(R.id.listViewID);
        databaseHelper = new DatabaseHelper(this);

        toastMessage("starting prepareListView method");
        prepareListView();
    }


    // Displaying data in the ListView
    public void prepareListView() {

        Cursor cursor = databaseHelper.displayData();
        ArrayList<String> listData = new ArrayList<>();
        while(cursor.moveToNext()){
            // get the value from the database in column 1
            // then add it to the ArrayList
            // index 1 means names
            listData.add(cursor.getString(1));
        }

        // create the list adapter and set the adapter
        ListAdapter listAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listData);
        list.setAdapter(listAdapter);

        // set an onItemClickListener to the ListView
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                // int i - is id number
                String name = adapterView.getItemAtPosition(i).toString();

                // get the id associated with that name
                Cursor cursor = databaseHelper.getItemID(name);

                int itemID = -1;
                while(cursor.moveToNext()){
                    // index 0 means id
                    itemID = cursor.getInt(0);
                }

                if( itemID > -1) {
                    Intent intent = new Intent(ListDataActivity.this, EditListViewRecords.class);
                    intent.putExtra("id", itemID);
                    //intent.putExtra("Name", name);
                    startActivity(intent);
                }
                else {
                    toastMessage("No ID associated with that name");
                }

            }
        });

    }

    private void toastMessage(String message){
        final Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        },500); // delay time
    }
}