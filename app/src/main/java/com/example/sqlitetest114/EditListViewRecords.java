package com.example.sqlitetest114;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class EditListViewRecords extends AppCompatActivity {

    Button btnUpdate, btnDelete;
    EditText nameEditText, ageEditText;
    DatabaseHelper databaseHelper;
    int selectedID;
    String selectedName;
    String selectedAge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_list_view_records);

        nameEditText = findViewById(R.id.name_editText);
        ageEditText = findViewById(R.id.age_editText);

        btnUpdate = findViewById(R.id.update_button);
        btnDelete = findViewById(R.id.delete_button);
        databaseHelper = new DatabaseHelper(this);

        // get the intent extra from the ListDataActivity
        Intent intent = getIntent();

        // now get the itemID we passed as an extra
        selectedID = intent.getIntExtra("id",-1); //NOTE: -1 is just the default value

        //now get the name we passed as an extra
        selectedName = intent.getStringExtra("Name");
        selectedAge = intent.getStringExtra("Age");


        // set the text to show the current selected name
        nameEditText.setText(selectedName);
        ageEditText.setText(selectedAge);

        // working with save button
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name = nameEditText.getText().toString();
                String age = ageEditText.getText().toString();

                if (!name.equals("")){
                    databaseHelper.updateName(selectedID, name, age);
                } else{
                    toastMessage("You must enter a name");
                }
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toastMessage("clicked in delete button");
            }
        });



    }

    // custom toastMessage
    private void toastMessage(String message){
        final Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        },500); // delay time
    }

}