package com.example.sqlitetest114;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText nameEditText, ageEditText;
    Button addButton, viewButton, listviewButton;

    DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nameEditText = (EditText) findViewById(R.id.name_editText);
        ageEditText = (EditText) findViewById(R.id.age_editText);
        addButton = (Button) findViewById(R.id.add_button);
        viewButton = (Button) findViewById(R.id.view_data);
        listviewButton = (Button) findViewById(R.id.list_view_button);

        databaseHelper = new DatabaseHelper(this);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = nameEditText.getText().toString();
                String age = ageEditText.getText().toString();

                // Simply add data without any toast message
                databaseHelper.insertData(name, age);

                /* ALSO YOU CAN TRY THIS
                if (nameEditText.length() != 0) {
                    AddData(name, age);
                    nameEditText.setText("");
                } else {
                    toastMessage("You must put something in the text field!");
                }
                 */

            }
        });

        // view records in alert dialog
        viewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // R.id.view_data (this is button id)
                if (v.getId() == R.id.view_data) {
                    // calling displayAllData method
                    Cursor cursor = databaseHelper.displayData();
                    if (cursor.getCount() == 0) {
                        // if there is no data
                        showData("Error", "No Data");
                        return;
                    }
                    StringBuffer stringBuffer = new StringBuffer();
                    while (cursor.moveToNext()) {
                        stringBuffer.append("ID     : " + cursor.getString(0) + "\n");
                        stringBuffer.append("Name   : " + cursor.getString(1) + "\n");
                        stringBuffer.append("Age    : " + cursor.getString(2) + "\n\n");
                        //stringBuffer.append("Gender : " + cursor.getString(3) + "\n\n");
                    }

                    showData("ResultSet", stringBuffer.toString());
                }
            }
        });

        // view records in list view
        listviewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListDataActivity.class);
                startActivity(intent);
                toastMessage("Clicked in list view button");
            }
        });

    }


    // printing list in alert dialog
    public void showData(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(true);
        builder.show();
    }

    /*
    public void AddData(String n, String a) {
        boolean insertData = databaseHelper.insertData(n, a);

        if (insertData) {
            toastMessage("Data Successfully Inserted!");
        } else {
            toastMessage("Something went wrong");
        }
    }
    */

    private void toastMessage(String message){
        final Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        },500); // delay time
    }

}